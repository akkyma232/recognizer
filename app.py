import argparse
from flask import Flask, request, flash, jsonify
from werkzeug.utils import secure_filename


parser = argparse.ArgumentParser()
parser.add_argument("protocol",
                    default='http',
                    choices=['http', 'websockets'])
parser.add_argument("--host", default='0.0.0.0', required=False)
parser.add_argument("--port", default=3000, required=False)

args = parser.parse_args()

if args.protocol == 'http':
    from http_run import run
elif args.protocol == 'websockets':
    from websockets_run import run
run(host=args.host, port=args.port)
