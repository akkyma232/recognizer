import os
import logging
import traceback
import json
import asyncio
import websockets
from filenames import DOWNLOADED_FILENAME, CONVERTED_FILENAME
from converters import convertFileToWav, convertWavToText

logging.basicConfig(level=logging.INFO)


async def recognize_audiofile(websocket, path):
    logging.info("File recognition start")
    
    try:
        with open(DOWNLOADED_FILENAME, "wb") as f:
            while (
                message := await websocket.recv()
            ) != "__EOF__":
                f.write(message)

        await websocket.send(json.dumps({'type': 'convert'}))
        await convertFileToWav(DOWNLOADED_FILENAME, CONVERTED_FILENAME)

        await websocket.send(json.dumps({'type': "recognize", 'value': 0}))
        async def processing_callback(progress):
            await websocket.send(json.dumps({'type': "recognize", 'value': progress}))
        recognition = await convertWavToText(CONVERTED_FILENAME, processing_callback)

        await websocket.send(json.dumps({'type': "done", 'tokens': recognition}))
    
    except Exception as e:
        logging.info("File recognition failed")
        raise e
    
    logging.info("File recognized successfully")


def run(host, port):
    loop = asyncio.get_event_loop()
    start_server = websockets.serve(recognize_audiofile, host, port)
    loop.run_until_complete(start_server)
    print(f"Listening start on {host}:{port}")
    loop.run_forever()


if __name__ == "__main__":
    run(host='0.0.0.0', port=3000)
