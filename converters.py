import os
import ffmpeg
import json
from vosk import Model, KaldiRecognizer

model = Model("./vosk-model-ru-0.22")


async def convertFileToWav(input_filename, output_filename, sample_rate=32000):
    (
        ffmpeg
        .input(input_filename)
        .output(output_filename, format="wav", ac=1, ar=sample_rate)
        .overwrite_output()
        .run(quiet=True)
    )


async def convertWavToText(filename, processing_callback=None):
    file_size = os.path.getsize(filename)

    with open(filename, "rb") as f:
        header = f.read(44)
        sample_rate = int.from_bytes(header[24:28], 'little', signed=False)
        rec = KaldiRecognizer(model, sample_rate)

        read_length = 0
        chunk_size = 1000000
        with open(filename, "rb") as f:
            while (chunk := f.read(chunk_size)) != b'':
                rec.AcceptWaveform(chunk)
                read_length += len(chunk)
                if processing_callback is not None:
                    await processing_callback(read_length / file_size)
    
    return json.loads(
        rec.FinalResult()
        )['result']
