import os
import signal
from flask import Flask, request, flash, jsonify
from werkzeug.utils import secure_filename
import asyncio
from filenames import DOWNLOADED_FILENAME, CONVERTED_FILENAME
from converters import convertFileToWav, convertWavToText


app = Flask(__name__)
app.secret_key = os.environ.get('FLASK_SECRET_KEY', 'badg3234rgwe512fa1')
loop = asyncio.get_event_loop()


@app.route('/recognize_audiofile', methods=['POST'])
def recognize_audiofile():
    if 'file' not in request.files:
        flash("No file part")
    file_ = request.files['file']
    if file_.filename == '':
        flash('No selected file')

    file_.save(DOWNLOADED_FILENAME)

    loop.run_until_complete(
        convertFileToWav(DOWNLOADED_FILENAME, CONVERTED_FILENAME)
    )

    recognition = loop.run_until_complete(
        convertWavToText(CONVERTED_FILENAME)
    )
    
    result = {
        'tokens': recognition
    }
    return jsonify(result)


def run(host, port):
    isDev = os.environ.get('FLASK_ENV') == "development"
    app.run(debug=isDev, host=host, port=port)


if __name__ == "__main__":
    run(host='0.0.0.0', port=3000)
