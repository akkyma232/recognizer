FROM python:3.9-slim
RUN groupadd --gid 2000 user \
    && useradd --uid 2000 --gid user --shell /bin/bash --create-home user

WORKDIR /app
ENV FLASK_ENV production
ENV FLASK_SECRET_KEY = "al4t;bflw32vfs23"

RUN apt update \
 && apt install -y ffmpeg \
 && apt clean


USER user

COPY vosk-model-ru-0.22 vosk-model-ru-0.22

COPY requirements.txt ./
RUN pip3 install -r requirements.txt

COPY filenames.py ./
COPY converters.py ./
COPY http_run.py ./
COPY websockets_run.py ./
COPY app.py ./

ENTRYPOINT ["python3", "app.py"]
