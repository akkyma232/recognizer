import os


FILES_FOLDER = "/tmp/recognizer"
os.makedirs(FILES_FOLDER, exist_ok=True)

DOWNLOADED_FILENAME = os.path.join(FILES_FOLDER, "downloaded_file.wav")
CONVERTED_FILENAME = os.path.join(FILES_FOLDER, "converted_file.wav")
